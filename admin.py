from django.contrib import admin

from tbot.models import BotConfig

admin.site.register(BotConfig)
