import telebot

from django.db import connection

from tbot.models import BotConfig


class Bot:
    def __init__(self):
        if 'tbot_botconfig' in connection.introspection.table_names() and BotConfig.objects.filter(is_active=True):
            bot_config = BotConfig.objects.get(is_active=True)
            self.server_url = bot_config.server_url
            self.bot = telebot.TeleBot(bot_config.token, threaded=False)
        else:
            self.bot = telebot.TeleBot('')

    def get_bot(self):
        return self.bot

    def get_server_url(self):
        return self.server_url

    @staticmethod
    def update(json_data):
        return telebot.types.Update.de_json(json_data)
