from django.urls import path

from tbot import handlers

urlpatterns = [
    path('get_hook/', handlers.get_hook, name="get_hook"),
]
